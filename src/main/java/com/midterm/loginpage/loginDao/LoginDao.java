package com.midterm.loginpage.loginDao;

import com.midterm.loginpage.model.Login;

import java.sql.SQLException;

public interface LoginDao {
    int save(Login login) throws ClassNotFoundException, SQLException;
    Login findOne(int id) throws ClassNotFoundException, SQLException;
}
