<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <title>Login</title>
</head>
<link rel="stylesheet" type="text/css" href="assets/css/style.css">
<body>
<%--<h1><%= "Hello World!" %>--%>
<%--</h1>--%>
<%--<br/>--%>
<%--<a href="hello-servlet">Hello Servlet</a>--%>
    <h1 align="center">Login Form</h1>

<form action="login" method="post">
    <div class="container">

        <label><b>Username/ E-mail</b></label>
        <input id="email" class="field" type="text" placeholder="Enter Username" name="email" required/>

        <label><b>Password</b></label>
        <input id="password" class="field" type="password" placeholder="Enter Password" name="password" required/><br>

        <button class="btn" type="submit">Submit</button>
    </div>
</form>

</body>
</html>